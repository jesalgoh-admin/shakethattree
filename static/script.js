$(function() {

  // Initialize the Reveal.js library with the default config options
  // See more here https://github.com/hakimel/reveal.js#configuration

  Reveal.initialize({
    history: true // Every slide will change the URL
  });

  // Connect to the socket

  var socket = io();

  // Variable initialization

  var form = $('form.login');
  var secretTextBox = form.find('input[type=text]');
  var presentation = $('.reveal');

  var qrcode = new QRCode(document.getElementById("qrcode"), {
    width: 200,
    height: 200
  });

  function makeCode() {
    // var elText = document.getElementById("text");
    var elText = "http://192.168.31.159:8080?key=kittens";

    // if (!elText.value) {
    // 	alert("Input a text");
    // 	elText.focus();
    // 	return;
    // }
    qrcode.makeCode(elText);
  }
  makeCode();

	function GetURLParameter(sParam) {
    var sPageURL = window.location.search.substring(1);
    var sURLVariables = sPageURL.split('&');
    for (var i = 0; i < sURLVariables.length; i++) {
      var sParameterName = sURLVariables[i].split('=');
      if (sParameterName[0] == sParam) {
        return sParameterName[1];
      }
    }
  }

  // $("#text").
  // 	on("blur", function () {
  // 		makeCode();
  // 	}).
  // 	on("keydown", function (e) {
  // 		if (e.keyCode == 13) {
  // 			makeCode();
  // 		}
  // 	});

  var key = GetURLParameter('key'), animationTimeout;
	if (typeof key !== 'undefined') {
		socket.emit('load', {
			key: key
		});
	}

  // When the page is loaded it asks you for a key and sends it to the server

  form.submit(function(e) {

    e.preventDefault();

    key = secretTextBox.val().trim();

    // If there is a key, send it to the server-side
    // through the socket.io channel with a 'load' event.

    if (key.length) {
      socket.emit('load', {
        key: key
      });
    }

  });

  // The server will either grant or deny access, depending on the secret key

  socket.on('access', function(data) {

    // Check if we have "granted" access.
    // If we do, we can continue with the presentation.

    if (data.access === "granted") {

      // Unblur everything
      presentation.removeClass('blurred');

      form.hide();

      var ignore = false;

      $(window).on('hashchange', function() {

        // Notify other clients that we have navigated to a new slide
        // by sending the "slide-changed" message to socket.io

        if (ignore) {
          // You will learn more about "ignore" in a bit
          return;
        }

        var hash = window.location.hash;

        socket.emit('slide-changed', {
          hash: hash,
          key: key
        });
      });

      socket.on('navigate', function(data) {

        // Another device has changed its slide. Change it in this browser, too:

        window.location.hash = data.hash;

        // The "ignore" variable stops the hash change from
        // triggering our hashchange handler above and sending
        // us into a never-ending cycle.

        ignore = true;

        setInterval(function() {
          ignore = false;
        }, 100);

      });

    } else {

      // Wrong secret key

      clearTimeout(animationTimeout);

      // Addding the "animation" class triggers the CSS keyframe
      // animation that shakes the text input.

      secretTextBox.addClass('denied animation');

      animationTimeout = setTimeout(function() {
        secretTextBox.removeClass('animation');
      }, 1000);

      form.show();
    }

  });
});
